## Project Credits
This document consists of a list of names of people who have contributed to this project in one way or another. Thank you to them and everyone else who has helped us.

* Takios
    - Testing the `install.sh` script and making sure that we didn't make any grave errors in the logic or syntax.
