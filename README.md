# The Cat Collective's Dotfiles

## Project Information
This repository hosts the autoinstall script for The Cat Collective's dotfiles located at https://gitlab.com/the-cat-collective/tcc-code/tcc-dotfiles.


## CAVEAT EMPTOR. 
**MAKE SURE TO READ THE SOURCE CODE FOR THE SCRIPT BEFORE RUNNING IT. THIS IS ALWAYS ALWAYS ALWAYS GOOD PRACTICE. WE ARE NOT VERY FAMILIAR WITH POSIX SHELL AND COULD HAVE MADE MISTAKES THAT COULD INEVITABLY LEAD TO DAMAGE TO OR EVEN DESTROY YOUR SYSTEM. YOU HAVE BEEN WARNED.**

## Other Caveats
* This project is currently **ONLY** compatible with Arch Linux and related distributions (such as Endeavour OS, Manjaro Linux, Arco Linux, Crystal Linux, etc...) due to the ease of acquiring some of the more esoteric dependencies via the AUR. Installing these dotfiles on other Linux distributions (such as Fedora and Ubuntu) may eventually be documented here.

* This guide also assumes that the user is familiar with configuring the software listed here, and so, will not contain a ricing guide or such to explain how to set these files beyond moving them around.

* If you decide to use the `install.sh` script, keep in mind that this will totally back up your `$HOME/.config` to another folder. This is TOTALLY INCOMPATIBLE with having other desktop environments installed, unless you want to manually manage all the chaos that will cause. Having multiple desktop environements and/or window managers is not something this script currently doesn't support and **will never** support.

## Getting Started
This guide assumes that you wish to clone this repo and replace your current configuration with it completely. If you want to pick and choose your components, go ahead, but we can't help you if something breaks. You are on your own.


## Cloning the repo
Run the following command to clone the sources:
```bash
git clone https://gitlab.com/the-cat-collective/tcc-code/tcc-dotfiles-autoinstaller
```

## Installation
Now for the fun part:
Back up your existing `$HOME/.config` if it exists and then copy over all the files here:
```bash
$ cd tcc-dotfiles-autoinstaller
$ chmod +x ./install.sh
$ ./install.sh
```
Follow the prompts in the installer.

## Project Structure
The root of the project contains subdirectories which individually contain configs for each piece of software. The root of the project also contains the `README` and `LICENSE` files.

## Licensing
* This project is licensed under the BSD 3-Clause License. Please refer to the [LICENSE](LICENSE) file for the full text of the project's license.

* For licensing information related to third-party components used in this project, please check the [LICENSING](LICENSING) file.

## Disclaimer
None