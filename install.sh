#!/bin/bash
#
# TCC Dotfiles auto-install script
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.

# =========================================================================== #
# Define all helper functions
# =========================================================================== #

# Function to create the Downloads folder if it doesn't exist
create_downloads_folder() {
    local download_folder
    download_folder="$HOME/Downloads"
    if [ ! -d "$download_folder" ]; then
        mkdir -p "$download_folder"
        echo "Downloads folder created at $download_folder."
    fi
}

# Function to create the .config folder if it doesn't exist
create_config_folder() {
    local config_folder
    config_folder="HOME/.config"
    if [ ! -d "$config_folder" ]; then
        mkdir -p "$config_folder"
        echo ".config folder created at $config_folder."
    fi
}

# Function to clone a Git repository to the user's Downloads folder and provide options when the repository already exists.
clone_and_move() {
    local repo_url
    local download_folder
    local repo_name
    local clone_path
    local config_folder
    
    repo_url="$1"
    download_folder="$HOME/Downloads"
    repo_name="$(basename "$repo_url" .git)"
    clone_path="$download_folder/$repo_name"
    config_folder="$HOME/.config"

    if [ -d "$clone_path" ]; then
        echo "Repository already exists in the Downloads folder."
        echo "What would you like to do?"
        PS3="Please enter your choice: "
        options=("Checkout the latest build" "Use the current build" "Abort")
        select choice in "${options[@]}"; do
            case $choice in
                1)
                    cd "$clone_path" || exit
                    git pull
                    echo "Latest build checked out."
                    # Move to the common logic after cloning
                    ;;
                2)
                    echo "Did nothing."
                    # Move to the common logic after cloning
                    ;;
                3)
                    echo "Aborted."
                    exit 1
                    ;;
                *)
                    echo "Invalid option. Please select a valid option (1, 2, or 3)."
                    ;;
            esac
        done
    else
        git clone "$repo_url" "$clone_path"
        if ! [ -d "$clone_path" ]; then
            echo "Failed to clone the repository."
            echo "What would you like to do?"
            PS3="Please enter your choice: "
            options=("Abort" "Abort with Revert")
            select choice in "${options[@]}"; do
                case $REPLY in
                    1)
                        echo "Aborted."
                        exit 1
                        ;;
                    2)
                        echo "Aborted with Revert."
                        revert_changes
                        ;;
                    *)
                        echo "Invalid option. Please select a valid option (1 or 2)."
                        ;;
                esac
            done
        else
            echo "Repository cloned successfully to $clone_path."
        fi

        # Common logic
        cd "$clone_path" || exit
        mv ./* "$config_folder"
    fi
}


# Function to handle install errors and offer to revert changes.
revert_changes() {
    echo "An error occurred. Do you want to revert changes and uninstall packages? (yes/no): "
    read -r revert_confirmation
    if [ "$revert_confirmation" = "yes" ]; then
        for package in "${installed_packages[@]}"; do
            if ! array_contains "$package" "${existing_packages[@]}"; then
                sudo pacman -Rns --noconfirm "$package"
            fi
        done

        if [ "${#existing_packages[@]}" -gt 0 ]; then
            echo "The following packages were already installed and are not removed:"
            for existing_package in "${existing_packages[@]}"; do
                echo "- $existing_package"
            done
        fi

        rm -rf $HOME/.config
        if [ -d "$HOME/.config.bak" ]; then
            cp -r $HOME/.config.bak $HOME/.config
        else
            :
        fi
        echo "Reverted changes (removed installed package and restored the user's .config)." >&2
        exit 1
    else
        echo "Exiting without reverting changes." >&2
        exit 1
    fi
}

# Function to handle file and/or io errors and offer to revert changes.
revert_file_changes() {
    echo "An error occurred. Do you want to revert file changes? (yes/no): "
    read -r revert_confirmation
    if [ "$revert_confirmation" = "yes" ]; then
        rm -rf .config
        cp -r ~/.config.bak ~/.config
        echo "Reverted changes (restored the user's ~/.config folder)." >&2
        exit 1
    else
        echo "Exiting without reverting changes. Warning: Your system may be in an incompatible or unstable state!" >&2
        exit 1
    fi
}

# Function to install prerequisite packages if they are not already installed.
install_package() {
    local package_name
    package_name="$1"

    # Check if the package is already installed
    if pacman -Q "$package_name" > /dev/null 2>&1; then
        echo "$package_name is already installed."
        existing_packages+=("$package_name")  # Add the already installed package to the array
    else
        echo "Installing $package_name..."
        paru -S --noconfirm "$package_name" || revert_changes
        installed_packages
        installed_packages+=("$package_name")
    fi
}

# Function to check if an element exists in an array
array_contains() {
    local search_element
    search_element="$1"
    shift
    local array
    array=("$@")
    for element in "${array[@]}"; do
        [[ "$element" == "$search_element" ]] && return 0
    done
    return 1
}

# =========================================================================== #
# Begin Program Logic
# =========================================================================== #

if ! command -v pacman > /dev/null 2>&1; then
    echo "This script is intended for Arch Linux only." >&2
    exit 1
fi

create_downloads_folder
create_config_folder
installed_packages=()
existing_packages=()

echo "Do you want to install the specified packages and configure them? (yes/no): "
read -r confirmation

if [ "$confirmation" != "yes" ]; then
    echo "Script execution aborted." >&2
    exit 1
fi

echo "Installing base-devel and git"
install_package "base-devel"
install_package "git"

if ! command -v paru > /dev/null 2>&1; then
    echo "paru is not installed. Installing it using git..."
    git clone https://aur.archlinux.org/paru.git || revert_changes
    (cd paru && makepkg -si) || revert_changes
    rm -rf paru || revert_changes
fi

packages=("blueberry"
        "bspwm" 
        "dunst" 
        "ttf-fira-code" 
        "ttf-firacode-nerd" 
        "flameshot" 
        "git" 
        "gnome-themes-extra"
        "kitty"
        "lxappearance" 
        "mate-polkit" 
        "noto-fonts" 
        "noto-fonts-cjk" 
        "paru" 
        "picom" 
        "playerctl" 
        "polybar" 
        "python-distutils-extra"
        "rofi" 
        "slock" 
        "sxhkd" 
        "xidlehook" 
        "xscreensaver"
        "zscroll-git"
)

for package in "${packages[@]}"; do
    install_package "$package"
done

if [ -d ~/.config.bak ]; then
    echo "A backup of ~/.config already exists. Do you want to overwrite it? (yes/no): "
    read -r overwrite_confirmation
    if [ "$overwrite_confirmation" = "yes" ]; then
        rm -rf ~/.config
        rm -rf ~/.config.bak
    else
        echo "Script execution aborted. Please manually handle the existing backup and re-run the script." >&2
        exit 1
    fi
fi

cp -r ~/.config ~/.config.bak

echo "WARNING, your .config has been backed up to .config.bak and the repo clone will now commence"

clone_and_move "https://gitlab.com/the-cat-collective/tcc-code/tcc-dotfiles"

echo "Script completed successfully."
